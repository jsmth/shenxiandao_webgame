package org.yunai.yfserver.message;

import org.junit.Test;
import org.yunai.yfserver.server.QueueMessageProcessor;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * MessageProcessor测试类
 * User: yunai
 * Date: 13-3-25
 * Time: 上午9:29
 */
public class MessageProcessorTest {

    @Test
    public void testLinkedBlockingQueue() throws InterruptedException {
        BlockingQueue queue = new LinkedBlockingQueue();
//System.out.println(queue.remainingCapacity());
//        queue.add(new Object());
//System.out.println(queue.remainingCapacity());
        System.out.println(queue.remainingCapacity());

//        queue.put();
        long t1 = System.currentTimeMillis();
        for (int i = 0; i < 1000000; i++) {
//            queue.add(new Object());
//            boolean result = queue.offer(new Object());
//            System.out.println("添加结果：" + result);
//            queue.add(new Object());
//            queue.put(new Object());
//            queue.offer(new Object());
        }
        System.out.println(System.currentTimeMillis() - t1);
    }

    @Test
    public void test() {
        final QueueMessageProcessor processor = new QueueMessageProcessor();
        processor.start();

//        for (int i = 0; i < 10; i++) {
//            Runnable runnable = new Runnable() {
//                @Override
//                public void run() {
//                    for (int i = 0; i < 10000; i++) {
//                        int x = new Random().nextInt(100);
//                        try {
//                            processor.put(x);
//                        } catch (InterruptedException e) {
//                            e.printStackTrace();
//                        }
//                    }
//                }
//            };
//            new Thread(runnable).start();
//            System.out.println("线程：" + i + "启动.");
//        }
    }

    public static void main(String[] args) {
        final QueueMessageProcessor processor = new QueueMessageProcessor();
        processor.start();

        for (int i = 0; i < 10; i++) {
            Runnable runnable = new Runnable() {
                @Override
                public void run() {
                    for (int i = 0; i < 10000; i++) {
//                        processor.put(new TestMessage());
                    }
                }
            };
            new Thread(runnable).start();
            System.out.println("线程：" + i + "启动.");
        }
    }
}
