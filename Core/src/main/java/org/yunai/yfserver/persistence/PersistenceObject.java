package org.yunai.yfserver.persistence;

import org.yunai.yfserver.persistence.orm.Entity;

import java.io.Serializable;

/**
 * 可持久化的业务对象实现此接口
 * User: yunai
 * Date: 13-4-6
 * Time: 上午11:24
 */
public interface PersistenceObject<ID extends Serializable, E extends Entity<ID>> {

    /**
     * 获得持久化对象的编号
     * @return 编号
     */
    ID getId();

    /**
     * 将实体对象转化成可持久化的业务对象
     * @param entity 实体对象
     */
    void fromEntity(E entity);

    /**
     * 将可持久化的业务对象转化为实体对象
     * @return 实体对象
     */
    E toEntity();

    /**
     * @return 是否在数据库中
     */
    boolean isInDB();

    /**
     * 设置是否在数据库中
     * @param inDB 是否在数据库
     */
    void setInDB(boolean inDB);

    /**
     * 获得该业务对象所属的对象<br />
     * 比如说，道具属于玩家，任务属于玩家<br />
     * 目前用于：数据持久化到DB时，使他们数据操作时是串行的
     *
     * @return 所属对象编号
     */
    Integer getUnitId();
}
