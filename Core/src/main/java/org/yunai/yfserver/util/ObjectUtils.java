package org.yunai.yfserver.util;

/**
 * 对象工具类
 * User: yunai
 * Date: 13-4-22
 * Time: 下午5:00
 */
public class ObjectUtils {

    /**
     * 判断两个数字是否相等<br />
     * 当且仅当a和b都不为空且相等
     *
     * @param a 数字A
     * @param b 数字B
     * @return 是否相等
     */
    public static boolean isEqual(Integer a, Integer b) {
        return a != null && b != null && a.equals(b);
    }

    /**
     * 判断两个数字是否相等<br />
     * 当且仅当a和b都不为空且相等
     *
     * @param a 数字A
     * @param b 数字B
     * @return 是否相等
     */
    public static boolean isEqual(Long a, Long b) {
        return a != null && b != null && a.equals(b);
    }

    /**
     * 判断两个对象是否相同<br />
     * 当且仅当a和b都不为空且相同
     *
     * @param a 对象A
     * @param b 对象B
     * @return 是否相同
     */
    public static boolean isSame(Object a, Object b) {
        return a != null && a == b;
    }

    /**
     * 判断整数是否为空或零
     *
     * @param a 整数
     * @return 是否为空或零
     */
    public static boolean isNull(Integer a) {
        return a == null && a == 0;
    }

    /**
     * 判断整数是否为空或零
     *
     * @param a 整数
     * @return 是否为空或零
     */
    public static boolean isNull(Long a) {
        return a == null || a == 0;
    }

    /**
     * 判断整数是否为空或零
     *
     * @param a 整数
     * @return 是否为空或零
     */
    public static boolean isNull(Short a) {
        return a == null || a == 0;
    }
}
