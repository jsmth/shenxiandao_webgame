package org.yunai.swjg.server.module.activity;

import org.yunai.yfserver.enums.IndexedEnum;
import org.yunai.yfserver.util.CollectionUtils;

import java.util.List;
import java.util.Set;

/**
 * 活动枚举
 * User: yunai
 * Date: 13-5-20
 * Time: 下午3:42
 */
public interface ActivityDef {

    /**
     * 活动状态枚举
     */
    public static enum Status implements IndexedEnum {
        /**
         * 准备中
         */
        PREPARE((byte) 1),
        /**
         * 进行中
         */
        PROCESS((byte) 2),
        /**
         * 结束
         */
        END((byte) 3);

        /**
         * 状态
         */
        private final byte index;
        /**
         * 前置状态
         */
        private Set<Status> preStatuses;

        static {
            PREPARE.preStatuses = CollectionUtils.asSet(END);
            PROCESS.preStatuses = CollectionUtils.asSet(PREPARE);
            END.preStatuses = CollectionUtils.asSet(PROCESS);
        }

        private Status(byte index) {
            this.index = index;
        }

        @Override
        public int getIndex() {
            return index;
        }

        /**
         * @return 状态编号
         */
        public byte getValue() {
            return index;
        }

        public static final List<Status> VALUES = Util.toIndexes(values());

        public static Status valueOf(Integer index) {
            return Util.valueOf(VALUES, index);
        }

        /**
         * @param preStatus 前状态
         * @return 是否能转化成该状态
         */
        public boolean valid(Status preStatus) {
            return preStatuses.contains(preStatus);
        }
    }
}
