package org.yunai.swjg.server.module.scene.core;

import org.yunai.yfserver.enums.IndexedEnum;

import java.util.List;

/**
 * 场景枚举接口
 * User: yunai
 * Date: 13-5-14
 * Time: 下午9:21
 */
public interface SceneDef {

    public static enum Type implements IndexedEnum {
        /**
         * 普通场景
         */
        NORMAL(1),
        /**
         * 单人副本
         */
        REP(2),
        /**
         * 活动副本
         */
        ACTIVITY(3);

        /**
         * 类型编号
         */
        private final int type;

        private Type(Integer type) {
            this.type = type;
        }

        @Override
        public int getIndex() {
            return type;
        }

        public static final List<Type> VALUES = Util.toIndexes(values());

        public static Type valueOf(Short index) {
            return Util.valueOf(VALUES, index);
        }
    }

}
