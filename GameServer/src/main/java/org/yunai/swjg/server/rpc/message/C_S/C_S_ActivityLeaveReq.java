package org.yunai.swjg.server.rpc.message.C_S;

import org.yunai.yfserver.message.*;
import org.yunai.yfserver.plugin.mina.command.AbstractMinaMessageCommand;
import org.yunai.swjg.server.core.message.GameMessage;
import org.yunai.yfserver.command.MessageDispatcher;
import org.yunai.yfserver.command.Command;

/**
 * 【21606】: 活动离开请求
 */
public class C_S_ActivityLeaveReq extends GameMessage {
    public static final short CODE = 21606;

    /**
     * 活动编号
     */
    private Byte actId;

    public C_S_ActivityLeaveReq() {
    }

    public C_S_ActivityLeaveReq(Byte actId) {
        this.actId = actId;
    }

    @Override
    public short getCode() {
        return CODE;
    }


@SuppressWarnings("unchecked")

@Override
    public void execute() {
        for (Command command : MessageDispatcher.getInstance().getCommands(CODE)) {
            ((AbstractMinaMessageCommand) command).execute(getSession(), this);
        }
    }

	public Byte getActId() {
		return actId;
	}

	public void setActId(Byte actId) {
		this.actId = actId;
	}

    public static class Decoder extends AbstractDecoder {
        private static Decoder decoder = new Decoder();

        public static Decoder getInstance() {
            return decoder;
        }

        public IStruct decode(ByteArray byteArray) {
            C_S_ActivityLeaveReq struct = new C_S_ActivityLeaveReq();
            struct.setActId(byteArray.getByte());
            return struct;
        }
    }

    public static class Encoder extends AbstractEncoder {
        private static Encoder encoder = new Encoder();

        public static Encoder getInstance() {
            return encoder;
        }

        public ByteArray encode(IStruct message) {
            C_S_ActivityLeaveReq struct = (C_S_ActivityLeaveReq) message;
            ByteArray byteArray = ByteArray.createNull(1);
            byteArray.create();
            byteArray.putByte(struct.getActId());
            return byteArray;
        }
    }
}