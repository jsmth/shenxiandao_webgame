package org.yunai.swjg.server.rpc.struct;

import org.yunai.yfserver.message.*;

/**
 * 竞技场对手信息
 */
public class StArenaOpponentInfo implements IStruct {
    /**
     * 等级
     */
    private Short level;
    /**
     * 昵称
     */
    private String nickname;
    /**
     * 排行
     */
    private Integer rank;
    /**
     * 性别
     */
    private Byte sex;
    /**
     * 职业
     */
    private Byte vocation;

    public StArenaOpponentInfo() {
    }

    public StArenaOpponentInfo(Short level, String nickname, Integer rank, Byte sex, Byte vocation) {
        this.level = level;
        this.nickname = nickname;
        this.rank = rank;
        this.sex = sex;
        this.vocation = vocation;
    }

	public Short getLevel() {
		return level;
	}

	public void setLevel(Short level) {
		this.level = level;
	}
	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	public Integer getRank() {
		return rank;
	}

	public void setRank(Integer rank) {
		this.rank = rank;
	}
	public Byte getSex() {
		return sex;
	}

	public void setSex(Byte sex) {
		this.sex = sex;
	}
	public Byte getVocation() {
		return vocation;
	}

	public void setVocation(Byte vocation) {
		this.vocation = vocation;
	}

    public static class Decoder extends AbstractDecoder {
        private static Decoder decoder = new Decoder();

        public static Decoder getInstance() {
            return decoder;
        }

        public IStruct decode(ByteArray byteArray) {
            StArenaOpponentInfo struct = new StArenaOpponentInfo();
            struct.setLevel(byteArray.getShort());
            struct.setNickname(getString(byteArray));
            struct.setRank(byteArray.getInt());
            struct.setSex(byteArray.getByte());
            struct.setVocation(byteArray.getByte());
            return struct;
        }
    }

    public static class Encoder extends AbstractEncoder {
        private static Encoder encoder = new Encoder();

        public static Encoder getInstance() {
            return encoder;
        }

        public ByteArray encode(IStruct message) {
            StArenaOpponentInfo struct = (StArenaOpponentInfo) message;
            ByteArray byteArray = ByteArray.createNull(8);
            byte[] nicknameBytes = convertString(byteArray, struct.getNickname());
            byteArray.create();
            byteArray.putShort(struct.getLevel());
            putString(byteArray, nicknameBytes);
            byteArray.putInt(struct.getRank());
            byteArray.putByte(struct.getSex());
            byteArray.putByte(struct.getVocation());
            return byteArray;
        }
    }
}