package org.yunai.swjg.server.module.battle.command;

import org.springframework.stereotype.Controller;
import org.yunai.swjg.server.core.service.GameMessageCommand;
import org.yunai.swjg.server.core.service.Online;
import org.yunai.swjg.server.rpc.message.C_S.C_S_BattleRepMonsterReq;

import javax.annotation.Resource;

/**
 * 与副本中怪物战斗命令
 * User: yunai
 * Date: 13-5-17
 * Time: 下午4:44
 */
@Controller
public class BattleRepMonsterCommand extends GameMessageCommand<C_S_BattleRepMonsterReq> {

    @Resource
    private BattleService battleService;

    @Override
    protected void execute(Online online, C_S_BattleRepMonsterReq msg) {
        battleService.battleRepMonster(online, msg.getId());
    }
}