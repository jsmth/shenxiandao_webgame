package org.yunai.swjg.server.rpc.message.S_C;

import org.yunai.yfserver.message.*;
import org.yunai.yfserver.plugin.mina.command.AbstractMinaMessageCommand;
import org.yunai.swjg.server.core.message.GameMessage;
import org.yunai.yfserver.command.MessageDispatcher;
import org.yunai.yfserver.command.Command;

/**
 * 【21611】: BOSS活动信息
 */
public class S_C_ActivityBossInfoResp extends GameMessage {
    public static final short CODE = 21611;

    /**
     * 活动编号
     */
    private Byte actId;
    /**
     * 参加人数
     */
    private Integer playerNum;
    /**
     * 开始倒计时，秒
     */
    private Integer processRemainTime;
    /**
     * 剩余时间，秒
     */
    private Integer remainTime;
    /**
     * 状态
     */
    private Byte status;

    public S_C_ActivityBossInfoResp() {
    }

    public S_C_ActivityBossInfoResp(Byte actId, Integer playerNum, Integer processRemainTime, Integer remainTime, Byte status) {
        this.actId = actId;
        this.playerNum = playerNum;
        this.processRemainTime = processRemainTime;
        this.remainTime = remainTime;
        this.status = status;
    }

    @Override
    public short getCode() {
        return CODE;
    }


@SuppressWarnings("unchecked")

@Override
    public void execute() {
        for (Command command : MessageDispatcher.getInstance().getCommands(CODE)) {
            ((AbstractMinaMessageCommand) command).execute(getSession(), this);
        }
    }

	public Byte getActId() {
		return actId;
	}

	public void setActId(Byte actId) {
		this.actId = actId;
	}
	public Integer getPlayerNum() {
		return playerNum;
	}

	public void setPlayerNum(Integer playerNum) {
		this.playerNum = playerNum;
	}
	public Integer getProcessRemainTime() {
		return processRemainTime;
	}

	public void setProcessRemainTime(Integer processRemainTime) {
		this.processRemainTime = processRemainTime;
	}
	public Integer getRemainTime() {
		return remainTime;
	}

	public void setRemainTime(Integer remainTime) {
		this.remainTime = remainTime;
	}
	public Byte getStatus() {
		return status;
	}

	public void setStatus(Byte status) {
		this.status = status;
	}

    public static class Decoder extends AbstractDecoder {
        private static Decoder decoder = new Decoder();

        public static Decoder getInstance() {
            return decoder;
        }

        public IStruct decode(ByteArray byteArray) {
            S_C_ActivityBossInfoResp struct = new S_C_ActivityBossInfoResp();
            struct.setActId(byteArray.getByte());
            struct.setPlayerNum(byteArray.getInt());
            struct.setProcessRemainTime(byteArray.getInt());
            struct.setRemainTime(byteArray.getInt());
            struct.setStatus(byteArray.getByte());
            return struct;
        }
    }

    public static class Encoder extends AbstractEncoder {
        private static Encoder encoder = new Encoder();

        public static Encoder getInstance() {
            return encoder;
        }

        public ByteArray encode(IStruct message) {
            S_C_ActivityBossInfoResp struct = (S_C_ActivityBossInfoResp) message;
            ByteArray byteArray = ByteArray.createNull(14);
            byteArray.create();
            byteArray.putByte(struct.getActId());
            byteArray.putInt(struct.getPlayerNum());
            byteArray.putInt(struct.getProcessRemainTime());
            byteArray.putInt(struct.getRemainTime());
            byteArray.putByte(struct.getStatus());
            return byteArray;
        }
    }
}