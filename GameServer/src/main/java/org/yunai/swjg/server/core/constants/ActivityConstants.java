package org.yunai.swjg.server.core.constants;

import org.yunai.yfserver.util.TimeUtils;

/**
 * 活动枚举
 * User: yunai
 * Date: 13-5-24
 * Time: 上午11:04
 */
public class ActivityConstants {

    /**
     * BOSS活动复活CD
     */
    public static final long BOSS_ACTIVITY_RELIVE = TimeUtils.MINUTE;

}
