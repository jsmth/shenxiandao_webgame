package org.yunai.swjg.server.entity;

import org.yunai.yfserver.persistence.orm.Entity;

/**
 * 玩家正在进行中的任务实体
 * User: yunai
 * Date: 13-5-9
 * Time: 下午7:00
 */
public class DoingQuestEntity implements Entity<Integer> {

    /**
     * 拓展字段对应类
     */
    public static class Prop {
        /**
         * 条件类型
         */
        private Integer type;
        /**
         * 主体
         */
        private Integer subject;
        /**
         * 数量
         */
        private Integer count;

        public Prop() {
        }

        public Prop(Integer type, Integer subject, Integer count) {
            this.type = type;
            this.subject = subject;
            this.count = count;
        }

        public Integer getType() {
            return type;
        }

        public void setType(Integer type) {
            this.type = type;
        }

        public Integer getSubject() {
            return subject;
        }

        public void setSubject(Integer subject) {
            this.subject = subject;
        }

        public Integer getCount() {
            return count;
        }

        public void setCount(Integer count) {
            this.count = count;
        }
    }

    /**
     * 编号
     */
    private Integer id;
    /**
     * 玩家编号
     */
    private Integer playerId;
    /**
     * 任务模版编号
     */
    private Integer questId;
    /**
     * 接受任务时间, 单位：毫秒
     */
    private Long acceptTime;
    /**
     * 拓展字段字符串<br />
     * 结构：[{
     *      type: 条件类型
     *      subject: 主体
     *      count: 数量
     * }]<br />
     * 对应类为：{@link Prop}
     */
    private String props;

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getPlayerId() {
        return playerId;
    }

    public void setPlayerId(Integer playerId) {
        this.playerId = playerId;
    }

    public Integer getQuestId() {
        return questId;
    }

    public void setQuestId(Integer questId) {
        this.questId = questId;
    }

    public Long getAcceptTime() {
        return acceptTime;
    }

    public void setAcceptTime(Long acceptTime) {
        this.acceptTime = acceptTime;
    }

    public String getProps() {
        return props;
    }

    public void setProps(String props) {
        this.props = props;
    }
}