package org.yunai.swjg.server.rpc.message.C_S;

import org.yunai.yfserver.message.*;
import java.util.List;
import org.yunai.swjg.server.rpc.struct.StArenaPlayerRank;
import org.yunai.swjg.server.rpc.struct.StArenaChallengeHistory;
import org.yunai.swjg.server.rpc.struct.StArenaOpponentInfo;
import org.yunai.yfserver.plugin.mina.command.AbstractMinaMessageCommand;
import org.yunai.swjg.server.core.message.GameMessage;
import org.yunai.yfserver.command.MessageDispatcher;
import org.yunai.yfserver.command.Command;

/**
 * 【21802】: 进入竞技场详细响应
 */
public class C_S_ArenaDetailResp extends GameMessage {
    public static final short CODE = 21802;

    /**
     * 挑战时间，单位（毫秒）
     */
    private Long bonusTime;
    /**
     * 挑战次数
     */
    private Integer challenge;
    /**
     * 挑战购买次数
     */
    private Integer challengeBuy;
    /**
     * 再次可以挑战时间，单位（毫秒）。0代表可以挑战
     */
    private Long challengeCD;
    /**
     * 排名
     */
    private Integer rank;
    /**
     * 英雄版数组
     */
    private List<StArenaPlayerRank> ranks;
    /**
     * 对手数组
     */
    private List<StArenaOpponentInfo> opponents;
    /**
     * 挑战历史数组
     */
    private List<StArenaChallengeHistory> challengeHistories;

    public C_S_ArenaDetailResp() {
    }

    public C_S_ArenaDetailResp(Long bonusTime, Integer challenge, Integer challengeBuy, Long challengeCD, Integer rank, List<StArenaPlayerRank> ranks, List<StArenaOpponentInfo> opponents, List<StArenaChallengeHistory> challengeHistories) {
        this.bonusTime = bonusTime;
        this.challenge = challenge;
        this.challengeBuy = challengeBuy;
        this.challengeCD = challengeCD;
        this.rank = rank;
        this.ranks = ranks;
        this.opponents = opponents;
        this.challengeHistories = challengeHistories;
    }

    @Override
    public short getCode() {
        return CODE;
    }


@SuppressWarnings("unchecked")

@Override
    public void execute() {
        for (Command command : MessageDispatcher.getInstance().getCommands(CODE)) {
            ((AbstractMinaMessageCommand) command).execute(getSession(), this);
        }
    }

	public Long getBonusTime() {
		return bonusTime;
	}

	public void setBonusTime(Long bonusTime) {
		this.bonusTime = bonusTime;
	}
	public Integer getChallenge() {
		return challenge;
	}

	public void setChallenge(Integer challenge) {
		this.challenge = challenge;
	}
	public Integer getChallengeBuy() {
		return challengeBuy;
	}

	public void setChallengeBuy(Integer challengeBuy) {
		this.challengeBuy = challengeBuy;
	}
	public Long getChallengeCD() {
		return challengeCD;
	}

	public void setChallengeCD(Long challengeCD) {
		this.challengeCD = challengeCD;
	}
	public Integer getRank() {
		return rank;
	}

	public void setRank(Integer rank) {
		this.rank = rank;
	}
	public List<StArenaPlayerRank> getRanks() {
		return ranks;
	}

	public void setRanks(List<StArenaPlayerRank> ranks) {
		this.ranks = ranks;
	}
	public List<StArenaOpponentInfo> getOpponents() {
		return opponents;
	}

	public void setOpponents(List<StArenaOpponentInfo> opponents) {
		this.opponents = opponents;
	}
	public List<StArenaChallengeHistory> getChallengeHistories() {
		return challengeHistories;
	}

	public void setChallengeHistories(List<StArenaChallengeHistory> challengeHistories) {
		this.challengeHistories = challengeHistories;
	}

    public static class Decoder extends AbstractDecoder {
        private static Decoder decoder = new Decoder();

        public static Decoder getInstance() {
            return decoder;
        }

        public IStruct decode(ByteArray byteArray) {
            C_S_ArenaDetailResp struct = new C_S_ArenaDetailResp();
            struct.setBonusTime(byteArray.getLong());
            struct.setChallenge(byteArray.getInt());
            struct.setChallengeBuy(byteArray.getInt());
            struct.setChallengeCD(byteArray.getLong());
            struct.setRank(byteArray.getInt());
		struct.setRanks(getMessageList(StArenaPlayerRank.Decoder.getInstance(), byteArray, StArenaPlayerRank.class));
		struct.setOpponents(getMessageList(StArenaOpponentInfo.Decoder.getInstance(), byteArray, StArenaOpponentInfo.class));
		struct.setChallengeHistories(getMessageList(StArenaChallengeHistory.Decoder.getInstance(), byteArray, StArenaChallengeHistory.class));
            return struct;
        }
    }

    public static class Encoder extends AbstractEncoder {
        private static Encoder encoder = new Encoder();

        public static Encoder getInstance() {
            return encoder;
        }

        public ByteArray encode(IStruct message) {
            C_S_ArenaDetailResp struct = (C_S_ArenaDetailResp) message;
            ByteArray byteArray = ByteArray.createNull(28);
            byte[][] ranksBytes = convertMessageList(byteArray, StArenaPlayerRank.Encoder.getInstance(), struct.getRanks());
            byte[][] opponentsBytes = convertMessageList(byteArray, StArenaOpponentInfo.Encoder.getInstance(), struct.getOpponents());
            byte[][] challengeHistoriesBytes = convertMessageList(byteArray, StArenaChallengeHistory.Encoder.getInstance(), struct.getChallengeHistories());
            byteArray.create();
            byteArray.putLong(struct.getBonusTime());
            byteArray.putInt(struct.getChallenge());
            byteArray.putInt(struct.getChallengeBuy());
            byteArray.putLong(struct.getChallengeCD());
            byteArray.putInt(struct.getRank());
            putMessageList(byteArray, ranksBytes);
            putMessageList(byteArray, opponentsBytes);
            putMessageList(byteArray, challengeHistoriesBytes);
            return byteArray;
        }
    }
}