package org.yunai.swjg.server.core.heartbeat;

/**
 * 心跳任务
 * User: yunai
 * Date: 13-5-24
 * Time: 上午9:35
 */
public interface HeartbeatTask extends Runnable {

    /**
     * @return 任务执行周期
     */
    long period();
}
