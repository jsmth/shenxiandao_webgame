package org.yunai.swjg.server.module.item;

import org.springframework.stereotype.Component;
import org.yunai.swjg.server.module.item.vo.Item;
import org.yunai.yfserver.async.IIoOperationService;
import org.yunai.yfserver.persistence.PersistenceObject;
import org.yunai.yfserver.persistence.async.DeletePersistenceObjectOperation;
import org.yunai.yfserver.persistence.async.SavePersistenceObjectOperation;
import org.yunai.yfserver.persistence.updater.PersistenceObjectUpdater;

import javax.annotation.Resource;

/**
 * 道具持久化更新器
 * User: yunai
 * Date: 13-4-10
 * Time: 上午12:46
 */
@Component
public class ItemUpdater implements PersistenceObjectUpdater {

    @Resource
    private ItemMapper itemMapper;
    @Resource
    private IIoOperationService ioOperationService;

    @Override
    public void save(PersistenceObject<?, ?> obj) {
        ioOperationService.asyncExecute(new SavePersistenceObjectOperation<>((Item) obj, itemMapper));
    }

    @Override
    public void delete(PersistenceObject<?, ?> obj) {
        ioOperationService.asyncExecute(new DeletePersistenceObjectOperation<>((Item) obj, itemMapper));
    }
}