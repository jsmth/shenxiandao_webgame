package org.yunai.swjg.server.rpc.message.S_C;

import org.yunai.yfserver.message.*;
import org.yunai.swjg.server.rpc.struct.StPlayerInfo;
import org.yunai.yfserver.plugin.mina.command.AbstractMinaMessageCommand;
import org.yunai.swjg.server.core.message.GameMessage;
import org.yunai.yfserver.command.MessageDispatcher;
import org.yunai.yfserver.command.Command;

/**
 * 【20604】: 场景中玩家加入请求
 */
public class S_C_ScenePlayerEnterReq extends GameMessage {
    public static final short CODE = 20604;

    /**
     * 玩家信息结构体
     */
    private StPlayerInfo playerEntity;

    public S_C_ScenePlayerEnterReq() {
    }

    public S_C_ScenePlayerEnterReq(StPlayerInfo playerEntity) {
        this.playerEntity = playerEntity;
    }

    @Override
    public short getCode() {
        return CODE;
    }


@SuppressWarnings("unchecked")

@Override
    public void execute() {
        for (Command command : MessageDispatcher.getInstance().getCommands(CODE)) {
            ((AbstractMinaMessageCommand) command).execute(getSession(), this);
        }
    }

	public StPlayerInfo getPlayerEntity() {
		return playerEntity;
	}

	public void setPlayerEntity(StPlayerInfo playerEntity) {
		this.playerEntity = playerEntity;
	}

    public static class Decoder extends AbstractDecoder {
        private static Decoder decoder = new Decoder();

        public static Decoder getInstance() {
            return decoder;
        }

        public IStruct decode(ByteArray byteArray) {
            S_C_ScenePlayerEnterReq struct = new S_C_ScenePlayerEnterReq();
            struct.setPlayerEntity((getMessage(StPlayerInfo.Decoder.getInstance(), byteArray, StPlayerInfo.class)));
            return struct;
        }
    }

    public static class Encoder extends AbstractEncoder {
        private static Encoder encoder = new Encoder();

        public static Encoder getInstance() {
            return encoder;
        }

        public ByteArray encode(IStruct message) {
            S_C_ScenePlayerEnterReq struct = (S_C_ScenePlayerEnterReq) message;
            ByteArray byteArray = ByteArray.createNull(0);
            byte[] playerEntityBytes = convertMessage(byteArray, StPlayerInfo.Encoder.getInstance(), struct.getPlayerEntity());
            byteArray.create();
            putMessage(byteArray, playerEntityBytes);
            return byteArray;
        }
    }
}