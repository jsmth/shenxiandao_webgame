package org.yunai.swjg.server.rpc.message.S_C;

import org.yunai.yfserver.message.*;
import org.yunai.swjg.server.rpc.struct.StQuestInfo;
import org.yunai.yfserver.plugin.mina.command.AbstractMinaMessageCommand;
import org.yunai.swjg.server.core.message.GameMessage;
import org.yunai.yfserver.command.MessageDispatcher;
import org.yunai.yfserver.command.Command;

/**
 * 【21003】: 任务更新响应
 */
public class S_C_QuestUpdateResp extends GameMessage {
    public static final short CODE = 21003;

    /**
     * 任务
     */
    private StQuestInfo quest;

    public S_C_QuestUpdateResp() {
    }

    public S_C_QuestUpdateResp(StQuestInfo quest) {
        this.quest = quest;
    }

    @Override
    public short getCode() {
        return CODE;
    }


@SuppressWarnings("unchecked")

@Override
    public void execute() {
        for (Command command : MessageDispatcher.getInstance().getCommands(CODE)) {
            ((AbstractMinaMessageCommand) command).execute(getSession(), this);
        }
    }

	public StQuestInfo getQuest() {
		return quest;
	}

	public void setQuest(StQuestInfo quest) {
		this.quest = quest;
	}

    public static class Decoder extends AbstractDecoder {
        private static Decoder decoder = new Decoder();

        public static Decoder getInstance() {
            return decoder;
        }

        public IStruct decode(ByteArray byteArray) {
            S_C_QuestUpdateResp struct = new S_C_QuestUpdateResp();
            struct.setQuest((getMessage(StQuestInfo.Decoder.getInstance(), byteArray, StQuestInfo.class)));
            return struct;
        }
    }

    public static class Encoder extends AbstractEncoder {
        private static Encoder encoder = new Encoder();

        public static Encoder getInstance() {
            return encoder;
        }

        public ByteArray encode(IStruct message) {
            S_C_QuestUpdateResp struct = (S_C_QuestUpdateResp) message;
            ByteArray byteArray = ByteArray.createNull(0);
            byte[] questBytes = convertMessage(byteArray, StQuestInfo.Encoder.getInstance(), struct.getQuest());
            byteArray.create();
            putMessage(byteArray, questBytes);
            return byteArray;
        }
    }
}