package org.yunai.swjg.server.module.scene.core.msg;

import org.slf4j.Logger;
import org.yunai.swjg.server.core.annotation.SceneThread;
import org.yunai.swjg.server.core.service.GameMessageProcessor;
import org.yunai.swjg.server.core.service.Online;
import org.yunai.swjg.server.core.service.OnlineContextService;
import org.yunai.swjg.server.module.scene.core.AbstractSceneService;
import org.yunai.swjg.server.module.scene.core.SceneCallable;
import org.yunai.yfserver.common.LoggerFactory;
import org.yunai.yfserver.message.sys.SysInternalMessage;
import org.yunai.yfserver.server.IMessageProcessor;
import org.yunai.yfserver.spring.BeanManager;

/**
 * 玩家离开场景<br />
 * 系统内部调用消息
 * <p/>
 * User: yunai
 * Date: 13-4-25
 * Time: 下午7:59
 */
public class SysPlayerLeaveScene
        extends SysInternalMessage {

    private static final Logger LOGGER = LoggerFactory.getLogger(LoggerFactory.Logger.scene, SysPlayerLeaveScene.class);


    private static IMessageProcessor messageProcessor;
    private static OnlineContextService onlineContextService;

    static {
        messageProcessor = BeanManager.getBean(GameMessageProcessor.class);
        onlineContextService = BeanManager.getBean(OnlineContextService.class);
    }

    /**
     * 玩家编号
     */
    private final Integer playerId;
    /**
     * 场景编号
     */
    private final Integer sceneId;
    /**
     * 场景线
     */
    private final Integer line;
    /**
     * 回调函数
     */
    private final SceneCallable callback;
    /**
     *
     */
    private AbstractSceneService sceneService;

    public SysPlayerLeaveScene(AbstractSceneService sceneService, Integer playerId, Integer sceneId, Integer line, SceneCallable callback) {
        this.sceneService = sceneService;
        this.playerId = playerId;
        this.sceneId = sceneId;
        this.line = line;
        this.callback = callback;
    }

    @Override
    @SceneThread
    public void execute() {
        Online online = onlineContextService.getPlayer(playerId);
        if (online == null) {
            LOGGER.error("[execute] [online:{} is not found].", playerId);
            return;
        }
        sceneService.handleLevelScene(online, sceneId, line);
        messageProcessor.put(new SysPlayerLeaveSceneResult(playerId, callback));
    }

    @Override
    public short getCode() {
        return 0;
    }
}
