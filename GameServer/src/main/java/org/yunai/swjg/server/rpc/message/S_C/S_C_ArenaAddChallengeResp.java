package org.yunai.swjg.server.rpc.message.S_C;

import org.yunai.yfserver.message.*;
import org.yunai.yfserver.plugin.mina.command.AbstractMinaMessageCommand;
import org.yunai.swjg.server.core.message.GameMessage;
import org.yunai.yfserver.command.MessageDispatcher;
import org.yunai.yfserver.command.Command;

/**
 * 【21804】: 增加竞技场挑战
 */
public class S_C_ArenaAddChallengeResp extends GameMessage {
    public static final short CODE = 21804;

    /**
     * 挑战次数
     */
    private Integer challenge;
    /**
     * 挑战购买次数
     */
    private Integer challengeBuy;

    public S_C_ArenaAddChallengeResp() {
    }

    public S_C_ArenaAddChallengeResp(Integer challenge, Integer challengeBuy) {
        this.challenge = challenge;
        this.challengeBuy = challengeBuy;
    }

    @Override
    public short getCode() {
        return CODE;
    }


@SuppressWarnings("unchecked")

@Override
    public void execute() {
        for (Command command : MessageDispatcher.getInstance().getCommands(CODE)) {
            ((AbstractMinaMessageCommand) command).execute(getSession(), this);
        }
    }

	public Integer getChallenge() {
		return challenge;
	}

	public void setChallenge(Integer challenge) {
		this.challenge = challenge;
	}
	public Integer getChallengeBuy() {
		return challengeBuy;
	}

	public void setChallengeBuy(Integer challengeBuy) {
		this.challengeBuy = challengeBuy;
	}

    public static class Decoder extends AbstractDecoder {
        private static Decoder decoder = new Decoder();

        public static Decoder getInstance() {
            return decoder;
        }

        public IStruct decode(ByteArray byteArray) {
            S_C_ArenaAddChallengeResp struct = new S_C_ArenaAddChallengeResp();
            struct.setChallenge(byteArray.getInt());
            struct.setChallengeBuy(byteArray.getInt());
            return struct;
        }
    }

    public static class Encoder extends AbstractEncoder {
        private static Encoder encoder = new Encoder();

        public static Encoder getInstance() {
            return encoder;
        }

        public ByteArray encode(IStruct message) {
            S_C_ArenaAddChallengeResp struct = (S_C_ArenaAddChallengeResp) message;
            ByteArray byteArray = ByteArray.createNull(8);
            byteArray.create();
            byteArray.putInt(struct.getChallenge());
            byteArray.putInt(struct.getChallengeBuy());
            return byteArray;
        }
    }
}