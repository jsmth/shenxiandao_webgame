package org.yunai.swjg.server.module.partner.command;

import org.springframework.stereotype.Controller;
import org.yunai.swjg.server.core.service.GameMessageCommand;
import org.yunai.swjg.server.core.service.Online;
import org.yunai.swjg.server.module.partner.PartnerService;
import org.yunai.swjg.server.rpc.message.C_S.C_S_PartnerInnListReq;

import javax.annotation.Resource;

/**
 * 伙伴客栈列表命令
 * User: yunai
 * Date: 13-5-31
 * Time: 下午7:09
 */
@Controller
public class PartnerInnListCommand extends GameMessageCommand<C_S_PartnerInnListReq> {

    @Resource
    private PartnerService partnerService;

    @Override
    protected void execute(Online online, C_S_PartnerInnListReq msg) {
        partnerService.innList(online);
    }
}
