package org.yunai.swjg.server.core.config;

/**
 * 游戏服务器配置
 * User: yunai
 * Date: 13-4-22
 * Time: 下午10:15
 */
public class GameServerConfig extends ServerConfig {

    public static final GameServerConfig INSTANCE = new GameServerConfig();

    private GameServerConfig() {}

    public static GameServerConfig getInstance() {
        return INSTANCE;
    }

    /**
     * 场景分线数量
     */
    @Deprecated
    public int SCENE_LINE_COUNT = 1;

    @Override
    public String getVersion() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void validate() {
        //To change body of implemented methods use File | Settings | File Templates.
    }
}
