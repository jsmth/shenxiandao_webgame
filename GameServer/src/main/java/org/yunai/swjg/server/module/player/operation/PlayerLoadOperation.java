package org.yunai.swjg.server.module.player.operation;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.yunai.swjg.server.core.service.Online;
import org.yunai.swjg.server.core.service.OnlineContextService;
import org.yunai.swjg.server.core.service.OnlineState;
import org.yunai.swjg.server.entity.PlayerEntity;
import org.yunai.swjg.server.module.player.PlayerExitReason;
import org.yunai.swjg.server.module.player.PlayerMapper;
import org.yunai.swjg.server.rpc.message.S_C.S_C_PlayerNoneResp;
import org.yunai.yfserver.async.IIoOperation;
import org.yunai.yfserver.async.IIoOperationService;
import org.yunai.yfserver.common.LoggerFactory;
import org.yunai.yfserver.spring.BeanManager;

/**
 * 玩家角色加载操作<br />
 * 该操作仅仅是获得下角色编号，不会将角色基本信息初始化到Online中去
 * User: yunai
 * Date: 13-3-29
 * Time: 上午10:50
 */
public class PlayerLoadOperation implements IIoOperation {

    private static final Logger LOGGER = LoggerFactory.getLogger(LoggerFactory.Logger.async, PlayerLoadOperation.class);

    private static PlayerMapper playerMapper;
    private static OnlineContextService onlineContextService;
    private static IIoOperationService ioOperationService;
    static {
        PlayerLoadOperation.playerMapper = BeanManager.getBean(PlayerMapper.class);
        PlayerLoadOperation.onlineContextService = BeanManager.getBean(OnlineContextService.class);
        PlayerLoadOperation.ioOperationService = BeanManager.getBean(IIoOperationService.class);
    }

    private volatile Online online;

    private volatile PlayerEntity playerEntity;
    private volatile boolean loadSuccess = false;

    public PlayerLoadOperation(Online online) {
        this.online = online;
    }

    @Override
    public State doStart() {
        return State.STARTED;
    }

    @Override
    public State doIo() {
        if (online.isConnected()) {
            try {
                playerEntity = playerMapper.selectPlayerByUidAndServerId(online.getUser().getId(), online.getServerId());
                loadSuccess = true;
            } catch (Exception e) {
                LOGGER.error("[doIo] [load user({}/{})'s player error: {}].", online.getUser().getId(), online.getServerId(),
                        ExceptionUtils.getStackTrace(e));
                loadSuccess = false;
                // 用户断开
                online.setExitReason(PlayerExitReason.SERVER_ERROR);
                online.disconnect();
            }
        }
        return State.IO_DONE;
    }

    @Override
    public State doFinish() {
        if (online.isConnected() && loadSuccess) {
            if (playerEntity == null) {
                online.setState(OnlineState.client_create_roleing);
                online.write(new S_C_PlayerNoneResp());
            } else {
                online.setState(OnlineState.load_roleing);
                // 执行玩家相关信息加载
                ioOperationService.asyncExecute(new PlayerInfoLoadOperation(online, playerEntity.getId()));
            }
        }
        return State.FINISHED;
    }
}
