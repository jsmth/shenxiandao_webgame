package org.yunai.swjg.server.rpc.struct;

import org.yunai.yfserver.message.*;

/**
 * 可见怪物结构体
 */
public class StVisibleMonsterInfo implements IStruct {
    /**
     * 怪物血当前值
     */
    private Integer hpCur;
    /**
     * 怪物血上限
     */
    private Integer hpMax;
    /**
     * 可见怪物编号，标识唯一
     */
    private Integer id;
    /**
     * 可见怪物模版编号
     */
    private Integer templateId;

    public StVisibleMonsterInfo() {
    }

    public StVisibleMonsterInfo(Integer hpCur, Integer hpMax, Integer id, Integer templateId) {
        this.hpCur = hpCur;
        this.hpMax = hpMax;
        this.id = id;
        this.templateId = templateId;
    }

	public Integer getHpCur() {
		return hpCur;
	}

	public void setHpCur(Integer hpCur) {
		this.hpCur = hpCur;
	}
	public Integer getHpMax() {
		return hpMax;
	}

	public void setHpMax(Integer hpMax) {
		this.hpMax = hpMax;
	}
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getTemplateId() {
		return templateId;
	}

	public void setTemplateId(Integer templateId) {
		this.templateId = templateId;
	}

    public static class Decoder extends AbstractDecoder {
        private static Decoder decoder = new Decoder();

        public static Decoder getInstance() {
            return decoder;
        }

        public IStruct decode(ByteArray byteArray) {
            StVisibleMonsterInfo struct = new StVisibleMonsterInfo();
            struct.setHpCur(byteArray.getInt());
            struct.setHpMax(byteArray.getInt());
            struct.setId(byteArray.getInt());
            struct.setTemplateId(byteArray.getInt());
            return struct;
        }
    }

    public static class Encoder extends AbstractEncoder {
        private static Encoder encoder = new Encoder();

        public static Encoder getInstance() {
            return encoder;
        }

        public ByteArray encode(IStruct message) {
            StVisibleMonsterInfo struct = (StVisibleMonsterInfo) message;
            ByteArray byteArray = ByteArray.createNull(16);
            byteArray.create();
            byteArray.putInt(struct.getHpCur());
            byteArray.putInt(struct.getHpMax());
            byteArray.putInt(struct.getId());
            byteArray.putInt(struct.getTemplateId());
            return byteArray;
        }
    }
}