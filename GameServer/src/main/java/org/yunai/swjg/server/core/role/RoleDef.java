package org.yunai.swjg.server.core.role;

/**
 * 角色
 * User: yunai
 * Date: 13-5-20
 * Time: 下午10:25
 */
public interface RoleDef {
    /**
     * 定义属性对象的类型
     * <p/>
     * User: yunai
     * Date: 13-5-2
     * Time: 下午8:37
     */
    public static enum PropertyType {

        /**
         * 角色一级属性
         */
        ROLE_PROPS_PRIMARY(1),
        /**
         * 角色二级属性
         */
        ROLE_PROPS_SECONDARY(2),
        /**
         * 角色在游戏过程中对客户端不可见的属性
         */
        ROLE_PROPS_FINAL(3),
        /**
         * 角色字符串属性
         */
        ROLE_PROPS_STRING(4);

        /**
         * 类型
         */
        private final int type;

        PropertyType(int type) {
            this.type = type;
        }

        public int getType() {
            return type;
        }
    }

    /**
     * 元素
     */
    public static enum Unit {
        /**
         * 玩家
         */
        PLAYER,
        /**
         * 伙伴
         */
        PARTNER,
        /**
         * 怪物
         */
        MONSTER;
    }
}
