package org.yunai.swjg.server.module.player.persistance;

import org.yunai.swjg.server.module.formation.FormationUpdater;
import org.yunai.swjg.server.module.formation.vo.Formation;
import org.yunai.swjg.server.module.item.ItemUpdater;
import org.yunai.swjg.server.module.item.vo.Item;
import org.yunai.swjg.server.module.partner.PartnerUpdater;
import org.yunai.swjg.server.module.partner.vo.Partner;
import org.yunai.swjg.server.module.player.PlayerUpdater;
import org.yunai.swjg.server.module.player.vo.Player;
import org.yunai.swjg.server.module.quest.DoingQuestUpdater;
import org.yunai.swjg.server.module.quest.FinishedQuestUpdater;
import org.yunai.swjg.server.module.quest.vo.DoingQuest;
import org.yunai.swjg.server.module.quest.vo.FinishedQuest;
import org.yunai.yfserver.persistence.PersistenceObject;
import org.yunai.yfserver.persistence.updater.AbstractDataUpdater;
import org.yunai.yfserver.persistence.updater.PersistenceObjectUpdater;
import org.yunai.yfserver.spring.BeanManager;

import java.util.HashMap;
import java.util.Map;

/**
 * Player数据更新器实现
 * User: yunai
 * Date: 13-4-9
 * Time: 下午8:56
 */
public class PlayerDataUpdater extends AbstractDataUpdater {

    public static final Map<Class<? extends PersistenceObject>, PersistenceObjectUpdater> updaters = new HashMap<>();
    static {
        updaters.put(Item.class, BeanManager.getBean(ItemUpdater.class));
        updaters.put(Player.class, BeanManager.getBean(PlayerUpdater.class));
        updaters.put(DoingQuest.class, BeanManager.getBean(DoingQuestUpdater.class));
        updaters.put(FinishedQuest.class, BeanManager.getBean(FinishedQuestUpdater.class));
        updaters.put(Partner.class, BeanManager.getBean(PartnerUpdater.class));
        updaters.put(Formation.class, BeanManager.getBean(FormationUpdater.class));
    }

    @Override
    protected void doUpdate(PersistenceObject po) {
        updaters.get(po.getClass()).save(po);
    }

    @Override
    protected void doDelete(PersistenceObject po) {
        updaters.get(po.getClass()).delete(po);
    }
}