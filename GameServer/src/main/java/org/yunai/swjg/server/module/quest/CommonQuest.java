package org.yunai.swjg.server.module.quest;

import org.yunai.swjg.server.core.constants.SysMessageConstants;
import org.yunai.swjg.server.module.player.vo.Player;
import org.yunai.swjg.server.module.quest.template.QuestTemplate;
import org.yunai.swjg.server.module.quest.vo.DoingQuest;
import org.yunai.swjg.server.module.quest.vo.FinishedQuest;
import org.yunai.swjg.server.rpc.message.S_C.S_C_SysMessageReq;

/**
 * （主线+支线）任务逻辑
 * User: yunai
 * Date: 13-5-9
 * Time: 上午8:22
 */
public class CommonQuest extends AbstractQuest {

    public CommonQuest(QuestTemplate template) {
        super(template);
    }

    /**
     * 是否能够接受任务<br />
     * <pre>
     *     1. 是否已经完成过
     * </pre>
     *
     * @param player    玩家信息
     * @param showError 是否提示错误消息给客户端
     * @return 是否能够接受任务
     */
    @Override
    protected boolean canAcceptImpl(Player player, boolean showError) {
        if (player.getQuestDiary().getFinishedQuestInfo(super.getId()) != null) {
            if (showError) {
                player.message(new S_C_SysMessageReq(SysMessageConstants.QUEST_ACCEPT_FINISHED));
            }
            return false;
        }
        return true;
    }

    @Override
    protected boolean canCancelImpl(Player player, boolean showError) {
        return true;
    }

    @Override
    protected boolean canFinishImpl(Player player, boolean showError) {
        return true;
    }

    /**
     * 保存一条任务完成信息，并加入完成的任务信息中
     *
     * @param player     玩家信息
     * @param doingQuest 该进行中任务的信息
     */
    @Override
    protected void finishImpl(Player player, DoingQuest doingQuest) {
        FinishedQuest finishedQuest = FinishedQuest.save(player, this, doingQuest.getAcceptTime(), timeService.now());
        player.getQuestDiary().addFinishedQuestInfo(finishedQuest);
    }

}
