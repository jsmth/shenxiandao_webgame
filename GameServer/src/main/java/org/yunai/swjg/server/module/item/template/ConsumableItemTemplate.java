package org.yunai.swjg.server.module.item.template;

import org.jumpmind.symmetric.csv.CsvReader;
import org.yunai.yfserver.util.CsvUtil;

import java.io.IOException;

/**
 * 消耗品道具模版<br />
 * 当消耗品为{@link org.yunai.swjg.server.module.item.ItemDef.Type#MEDICINE_CONSUMABLE}时，
 * <pre>
 *     param1: 加成属性类型。1-武力，2-绝技，3-法力
 *     param2：加成等级。加成的值为y = (2 + a - b) * 5，其中a为加成等级，b为该等级第几个丹药。属性加成是会递减的。
 * </pre>
 * User: yunai
 * Date: 13-6-3
 * Time: 下午7:33
 */
public class ConsumableItemTemplate extends ItemTemplate {

    /**
     * 使用拓展参数1
     */
    private int param1;
    /**
     * 使用拓展参数2
     */
    private int param2;
    /**
     * 使用拓展参数3
     */
    private int param3;

    public int getParam1() {
        return param1;
    }

    public void setParam1(int param1) {
        this.param1 = param1;
    }

    public int getParam2() {
        return param2;
    }

    public void setParam2(int param2) {
        this.param2 = param2;
    }

    public int getParam3() {
        return param3;
    }

    public void setParam3(int param3) {
        this.param3 = param3;
    }

    // ==================== 非set/get方法 ====================
    @Override
    protected void genTemplate(CsvReader reader) throws IOException {
        param1 = CsvUtil.getInt(reader, "param1", 0);
        param2 = CsvUtil.getInt(reader, "param2", 0);
        param3 = CsvUtil.getInt(reader, "param3", 0);
    }

    @Override
    protected void check() {
        // 当类型为丹药时，param1的范围为[1,3]
    }
}