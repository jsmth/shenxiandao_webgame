package org.yunai.swjg.server.module.battle.unit;

import org.yunai.swjg.server.module.battle.BattleDef;
import org.yunai.swjg.server.module.battle.BattleUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

/**
 * 战斗队伍
 * User: yunai
 * Date: 13-5-23
 * Time: 下午4:39
 */
public class BattleTeam {

    private final BattleDef.Team team;
    private final List<BattleUnit> units = new ArrayList<>(5);
    private final TreeMap<Integer, BattleUnit> alives = new TreeMap<>();

    public BattleTeam(BattleDef.Team team) {
        this.team = team;
    }

    public void addUnit(BattleUnit unit) {
        if (unit.isAlive()) {
            units.add(unit);
            alives.put(unit.getUnitIndex(), unit);
        }
    }

    public List<BattleUnit> getUnits() {
        return units;
    }

    public TreeMap<Integer, BattleUnit> getAlives() {
        return alives;
    }

    public BattleDef.Team getTeam() {
        return team;
    }

    /**
     * @return 检查alives数组是否有元素
     */
    public boolean isAlive() {
        return !alives.isEmpty();
    }

    /**
     * 深度检查alives数组是否元素有存活
     *
     * @return 是否有存活
     */
    public boolean checkAlive() {
        for (BattleUnit unit : alives.values()) {
            if (unit.isAlive()) {
                return true;
            }
        }
        return false;
    }

    /**
     * @param hang 行
     * @return 指定行所有存活的战斗单元
     */
    public List<BattleUnit> getHangAlives(int hang) {
        List<BattleUnit> result = new ArrayList<>(BattleUtils.LIE_COUNT);
        int beginIndex = (team == BattleDef.Team.ATTACK ? BattleUtils.INDEX_BEGIN_ATTACK_TEAM
                : BattleUtils.INDEX_BEGIN_DEFENSE_TEAM) + (hang - 1);
        for (int i = 0; i < BattleUtils.LIE_COUNT; i++) {
            BattleUnit unit = alives.get(beginIndex + i * BattleUtils.HANG_COUNT);
            if (unit != null && unit.isAlive()) {
                result.add(unit);
            }
        }
        return result;
    }

    /**
     * @param lie 列
     * @return 指定列所有存活的战斗单元
     */
    public List<BattleUnit> getLieAlives(int lie) {
        List<BattleUnit> result = new ArrayList<>(BattleUtils.LIE_COUNT);
        int beginIndex = (team == BattleDef.Team.ATTACK ? BattleUtils.INDEX_BEGIN_ATTACK_TEAM
                : BattleUtils.INDEX_BEGIN_DEFENSE_TEAM) + (lie - 1) * BattleUtils.HANG_COUNT;
        for (int i = 0; i < BattleUtils.HANG_COUNT; i++) {
            BattleUnit unit = alives.get(beginIndex + i);
            if (unit != null && unit.isAlive()) {
                result.add(unit);
            }
        }
        return result;
    }

    /**
     * @return 所有存活的战斗单元
     */
    public List<BattleUnit> getAllAlives() {
        List<BattleUnit> result = new ArrayList<>(alives.size());
        for (BattleUnit unit : alives.values()) {
            if (unit.isAlive()) {
                result.add(unit);
            }
        }
        return result;
    }

    public static void main(String[] args) {
//        new BattleTeam(BattleDef.Team.ATTACK).getHangAlives(1);
//        new BattleTeam(BattleDef.Team.ATTACK).getHangAlives(2);
//        new BattleTeam(BattleDef.Team.ATTACK).getHangAlives(3);
//        new BattleTeam(BattleDef.Team.DEFENSE).getHangAlives(1);
//        new BattleTeam(BattleDef.Team.DEFENSE).getHangAlives(2);
//        new BattleTeam(BattleDef.Team.DEFENSE).getHangAlives(3);

//        new BattleTeam(BattleDef.Team.ATTACK).getLieAlives(1);
//        new BattleTeam(BattleDef.Team.ATTACK).getLieAlives(2);
//        new BattleTeam(BattleDef.Team.ATTACK).getLieAlives(3);
//        new BattleTeam(BattleDef.Team.ATTACK).getLieAlives(4);
//        new BattleTeam(BattleDef.Team.DEFENSE).getLieAlives(1);
//        new BattleTeam(BattleDef.Team.DEFENSE).getLieAlives(2);
//        new BattleTeam(BattleDef.Team.DEFENSE).getLieAlives(3);
//        new BattleTeam(BattleDef.Team.DEFENSE).getLieAlives(4);
    }
}
