package org.yunai.swjg.server.module.quest;

import org.springframework.stereotype.Component;
import org.yunai.swjg.server.module.quest.vo.FinishedQuest;
import org.yunai.yfserver.async.IIoOperationService;
import org.yunai.yfserver.persistence.PersistenceObject;
import org.yunai.yfserver.persistence.async.SavePersistenceObjectOperation;
import org.yunai.yfserver.persistence.updater.PersistenceObjectUpdater;

import javax.annotation.Resource;

/**
 * 玩家完成的任务的数据持久更新器
 * User: yunai
 * Date: 13-5-10
 * Time: 下午7:34
 */
@Component
public class FinishedQuestUpdater implements PersistenceObjectUpdater {

    @Resource
    private FinishedQuestMapper finishedQuestMapper;
    @Resource
    private IIoOperationService ioOperationService;

    @Override
    public void save(PersistenceObject<?, ?> obj) {
        ioOperationService.asyncExecute(new SavePersistenceObjectOperation<>((FinishedQuest) obj, finishedQuestMapper));
    }

    @Override
    public void delete(PersistenceObject<?, ?> obj) {
        throw new UnsupportedOperationException("完成的任务不支持delete.");
    }
}
