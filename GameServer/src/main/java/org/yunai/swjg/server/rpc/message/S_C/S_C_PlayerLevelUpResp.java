package org.yunai.swjg.server.rpc.message.S_C;

import org.yunai.yfserver.message.*;
import org.yunai.yfserver.plugin.mina.command.AbstractMinaMessageCommand;
import org.yunai.swjg.server.core.message.GameMessage;
import org.yunai.yfserver.command.MessageDispatcher;
import org.yunai.yfserver.command.Command;

/**
 * 【21205】: 玩家等级上升消息
 */
public class S_C_PlayerLevelUpResp extends GameMessage {
    public static final short CODE = 21205;

    /**
     * 升级后的等级
     */
    private Short level;

    public S_C_PlayerLevelUpResp() {
    }

    public S_C_PlayerLevelUpResp(Short level) {
        this.level = level;
    }

    @Override
    public short getCode() {
        return CODE;
    }


@SuppressWarnings("unchecked")

@Override
    public void execute() {
        for (Command command : MessageDispatcher.getInstance().getCommands(CODE)) {
            ((AbstractMinaMessageCommand) command).execute(getSession(), this);
        }
    }

	public Short getLevel() {
		return level;
	}

	public void setLevel(Short level) {
		this.level = level;
	}

    public static class Decoder extends AbstractDecoder {
        private static Decoder decoder = new Decoder();

        public static Decoder getInstance() {
            return decoder;
        }

        public IStruct decode(ByteArray byteArray) {
            S_C_PlayerLevelUpResp struct = new S_C_PlayerLevelUpResp();
            struct.setLevel(byteArray.getShort());
            return struct;
        }
    }

    public static class Encoder extends AbstractEncoder {
        private static Encoder encoder = new Encoder();

        public static Encoder getInstance() {
            return encoder;
        }

        public ByteArray encode(IStruct message) {
            S_C_PlayerLevelUpResp struct = (S_C_PlayerLevelUpResp) message;
            ByteArray byteArray = ByteArray.createNull(2);
            byteArray.create();
            byteArray.putShort(struct.getLevel());
            return byteArray;
        }
    }
}