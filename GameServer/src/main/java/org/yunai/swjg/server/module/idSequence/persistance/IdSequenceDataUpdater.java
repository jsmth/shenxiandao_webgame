package org.yunai.swjg.server.module.idSequence.persistance;

import org.springframework.stereotype.Component;
import org.yunai.swjg.server.entity.IdSequenceEntity;
import org.yunai.swjg.server.module.idSequence.IdSequence;
import org.yunai.swjg.server.module.idSequence.IdSequenceMapper;
import org.yunai.yfserver.async.IIoOperationService;
import org.yunai.yfserver.persistence.PersistenceObject;
import org.yunai.yfserver.persistence.async.SavePersistenceObjectOperation;
import org.yunai.yfserver.persistence.updater.AbstractDataUpdater;

import javax.annotation.Resource;

/**
 * Created with IntelliJ IDEA.
 * User: yunai
 * Date: 13-4-11
 * Time: 下午2:14
 */
@Component
public class IdSequenceDataUpdater extends AbstractDataUpdater {

    @Resource
    private IIoOperationService ioOperationService;

    @Resource
    private IdSequenceMapper idSequenceMapper;

    @Override
    protected void doUpdate(PersistenceObject po) {
        ioOperationService.asyncExecute(new SavePersistenceObjectOperation<IdSequenceEntity, IdSequence>((IdSequence) po, idSequenceMapper));
    }

    @Override
    protected void doDelete(PersistenceObject po) {
    }
}
