package quartz;

import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;

import java.util.Date;

import static org.quartz.JobBuilder.newJob;
import static org.quartz.TriggerBuilder.newTrigger;

/**
 * Quartz Hello World
 * User: yunai
 * Date: 13-5-25
 * Time: 上午3:36
 */
public class QuartzTest {

    public static void main(String[] args) throws SchedulerException {
        // 通过SchedulerFactory获取一个调度器实例
        SchedulerFactory sf = new StdSchedulerFactory();

        Scheduler sched = sf.getScheduler();

        Date runTime = DateBuilder.evenMinuteDate(new Date());
        System.out.println(runTime);

        // 通过过JobDetail封装HelloJob，同时指定Job在Scheduler中所属组及名称，这里，组名为group1，而名称为job1。
//        JobDetail job = newJob(HelloJobX.class).withIdentity("job1", "group1").build();
        JobDetail job = newJob(HelloJobX.class).withIdentity("job1", "group1").build();

        // 创建一个SimpleTrigger实例，指定该Trigger在Scheduler中所属组及名称。
        // 接着设置调度的时间规则.当前时间运行
        Trigger trigger = newTrigger().withIdentity("trigger1", "group1").startAt(runTime).build();

        // 注册并进行调度
        sched.scheduleJob(job, trigger);

        // 启动调度器
        sched.start();

        try {
            //当前线程等待65秒
            Thread.sleep(65L * 1000L);
        } catch (Exception e) {

        }

        //调度器停止运行
        sched.shutdown(true);

//        log.error("结束运行。。。。");
    }

//    public static void main(String args[]) {
//        // 得到可用客户端处理调度程序的工厂
//        SchedulerFactory schedFact = new StdSchedulerFactory();
//        // 高度程序接口,控制JobDeail和Trigger
//        try {
//            Scheduler sched = schedFact.getScheduler();
//            // 表示给定工作类的详细信息
//            JobDetail jobDetail = new JobDetail("myJob", null, quartz.HelloJob.class);
//            // 触发器
//            Trigger trigger = TriggerUtils.makeSecondlyTrigger();
//            // 设置开始时间,这时设置的是每一秒执行一次
//            trigger.setStartTime(TriggerUtils.getEvenSecondDateBefore(new Date()));
//            // 必须设置,不能为空
//            trigger.setName("firstTrigger");
//            // 为调度程序设置具体工作和触发器
//            sched.scheduleJob(jobDetail, trigger);
//            // fire
//            sched.start();
//        } catch (SchedulerException e) {
//            e.printStackTrace();
//        }
//    }

    public static class HelloJobX implements Job {

        public HelloJobX() {

        }

        public void execute(JobExecutionContext context)
                throws JobExecutionException {

            System.err.println(" 咫尺天涯: " + new Date());


        }
    }
}

